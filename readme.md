## About

This is the home page of the BullCow game made following the [The Unreal Engine Developer Course](https://www.udemy.com/unrealcourse/learn/v4/overview)
 on [Udemy](https://www.udemy.com).

Feel free to play the game and read the source code. Any feedback is welcomed.

## Get Started

Download the latest version of the game or a copy of the source code and [Microsoft Visual Studio](https://www.visualstudio.com/)
project files from the master branch.

## Builds

If you want to build your own copy of the game you can use the [Microsoft Visual Studio](https://www.visualstudio.com/)
project files with the source code and build it through [Microsoft Visual Studio](https://www.visualstudio.com/) or you
can use your own C++ compiler such as [MinGW](http://www.mingw.org/).

## Notes

Currently only fully compatible with Windows 7+