/*
*	Created by: Dan
*	Created: 13/07/2017
*
*	Last Modified By: Dan
*	Last Modified: 14/07/2017
*
*	Description: Simple utility header class for BullCowGame
*/

#pragma once
#include <string>

class Utility {
	public:
		static bool stringEqualsIgnoreCase(std::string info, std::string compareTo);

	private:
		static bool regionMatches(bool ignoreCase, std::string info, std::string compareTo, int toffset, int ooffset, int len);
};

