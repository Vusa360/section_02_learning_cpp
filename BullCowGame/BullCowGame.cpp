/*
*	Created by: Dan
*	Created: 11/07/2017
*
*	Last Modified By: Dan
*	Last Modified: 16/07/2017
*
*	Description: Handles all the game logic of the BullCow game
*/

#include "BullCowGame.h"
#include "Utility.h"
#include <string>
#include <cmath>
#include <vector>
#include <regex>
#include <random>

using FString = std::string;
using int32 = int;

BullCowGame::BullCowGame() {
	defineDictionaries();
	wordDif = STARTING_WORD_DIFFICULTY;
	guessDif = STARTING_GUESS_DIFFICULTY;
}

void BullCowGame::reset() {
	gameWon = false;
	setWordDif(wordDif);

	switch(wordDif) {
		case Dificulty::EASY:
			hiddenWord = chooseWord(easyDic);
			break;
		case Dificulty::MEDIUM:
			hiddenWord = chooseWord(medDic);
			break;
		case Dificulty::HARD:
			hiddenWord = chooseWord(hardDic);
			break;
		default:
			hiddenWord = chooseWord(easyDic);
			break;
	}

	maxTries = calcMaxTries(hiddenWord, guessDif);
	currentTry = 1;
}

void BullCowGame::setWordDif(Dificulty dif) {
	wordDif = dif;
}

void BullCowGame::setGuessDif(Dificulty dif) {
	guessDif = dif;
}

Dificulty BullCowGame::getWordDif() {
	return wordDif;
}

Dificulty BullCowGame::getGuessDif() {
	return guessDif;
}

FString BullCowGame::convertToLowerCase(FString word) {
	std::transform(word.begin(), word.end(), word.begin(), ::tolower);
	return word;
}

void BullCowGame::defineDictionaries() {
	easyDic = {
		"Raw",
		"Pot",
		"Easy",
		"Train",
		"Computer",
		"Blue",
		"Red",
		"Prey",
		"Grey",
		"Pray",
		"Smart",
		"Cat",
		"Dog",
		"Judge",
		"Orange",
		"Game",
		"Phone",
		"Mobile",
		"Powder",
		"Jumper",
		"Suit"
	};

	medDic = {
		"Pot",
		"Median",
		"Smite",
		"Disgraceful",
		"Lumberjack",
		"Background",
		"Lice",
		"Peril",
		"Fake",
		"Atmospheric",
		"Regulation",
		"Trampoline",
		"Steam",
		"Mobile",
		"Jucies",
		"Grouse",
		"Wretch",
		"Modest",
		"Spy",
		"Broad",
		"Problem"
	};

	hardDic = {
		"Subdermatoglyphic",
		"Uncopyrightable",
		"Dermatoglyphics",
		"Hydropneumatics",
		"Misconjugatedly",
		"Hydromagnetic",
		"Unmaledictory",
		"Kymograph",
		"Lubavitchers",
		"Abolishment",
		"Exclusionary",
		"Fluoridates",
		"Infamous",
		"Zeal",
		"Xylography",
		"Juxtapose",
		"Misanthrope",
		"Phlegmatic",
		"Imprudent",
		"Subformative",
		"Pseudomythical"
	};
}

FString BullCowGame::chooseWord(std::vector<FString> dic) {
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist(0, dic.size() - 1);
	
	return dic.at(dist(rng));
}

BullCowCount BullCowGame::submitValidGuess(FString guess) { // Always recieves a valid guess
	currentTry++;
	BullCowCount bullCowCount;

	int32 wordLength = hiddenWord.length(); //As it always recieves a valid guess
											//this is a safe operation

	if(Utility::stringEqualsIgnoreCase(guess, hiddenWord)) {
		bullCowCount.bulls = wordLength;
		gameWon = true;
	} else {
		FString check = convertToLowerCase(guess);
		FString checkAgainst = convertToLowerCase(hiddenWord);

		for(int32 i = 0; i < wordLength; i++) {
			if(check[i] == checkAgainst[i]) {
				bullCowCount.bulls++;
			} else {
				for(int32 j = 0; j < wordLength; j++) {
					if(check[i] == checkAgainst[j])
						bullCowCount.cows++;
				}
			}
		}
	}

	return bullCowCount;
}

FString BullCowGame::getHiddenWord() {
	return hiddenWord;
}

int32 BullCowGame::getMaxTries() const {
	return maxTries;
}

int32 BullCowGame::getCurrentTry() const {
	return currentTry;
}

bool BullCowGame::isGameWon() const {
	return gameWon;
}

int32 BullCowGame::calcMaxTries(FString word, Dificulty dif) {
	int32 tries = word.length();
	if(dif == Dificulty::EASY) {
		tries = (int32) (std::round(std::pow(tries, 1.5)));
	} else if(dif == Dificulty::MEDIUM) {
		tries = (int32) (std::round(std::pow(tries, 1.25)));
		tries += 2;
	} else {
		tries = (int32) (std::round(std::pow(tries, 1.1)));
		tries ++;
	}
	return tries;
}

GuessValidity BullCowGame::isValidGuess(FString guess) const {
	const std::regex alpha("^[a-z]*$", std::regex_constants::ECMAScript | std::regex_constants::icase);
	const std::regex isogram("^(?:([a-z])(?!.*\\1))*$", std::regex_constants::ECMAScript | std::regex_constants::icase);

	if(guess.length() != hiddenWord.length())
		return GuessValidity::INCORRECT_LENGTH;
	else if(!std::regex_match(guess, alpha))
		return GuessValidity::NONE_ALPHA_CHARS;
	else if(!std::regex_match(guess, isogram))
		return GuessValidity::NOT_ISOGRAM;
	else
		return GuessValidity::OK;
}