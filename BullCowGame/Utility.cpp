/*
*	Created by: Dan
*	Created: 13/07/2017
*
*	Last Modified By: Dan
*	Last Modified: 14/07/2017
*
*	Description: Simple utility class definitions for BullCowGame
*/

#include "Utility.h"
#include <string>
#include <ctype.h>
#include <vector>

bool Utility::stringEqualsIgnoreCase(std::string info, std::string compareTo) {
	return (info == compareTo) ? true
		: (!compareTo.empty())
		&& (compareTo.length() == info.length())
		&& regionMatches(true, info, compareTo, 0, 0, info.length());
}

bool Utility::regionMatches(bool ignoreCase, std::string info, std::string compareTo, int toffset, int ooffset, int len) {
	std::vector<char> infoStr(info.c_str(), info.c_str() + info.size() + 1);
	std::vector<char> compStr(compareTo.c_str(), compareTo.c_str() + compareTo.size() + 1);
	char *ta = &infoStr[0];
	int to = toffset;
	char *pa = &compStr[0];
	int po = ooffset;
	// Note: toffset, ooffset, or len might be near -1>>>1.
	if((ooffset < 0) || (toffset < 0)
	   || (toffset > (long) info.length() - len)
	   || (ooffset > (long) compareTo.length() - len)) {
		return false;
	}
	while(len-- > 0) {
		char c1 = ta[to++];
		char c2 = pa[po++];
		if(c1 == c2) {
			continue;
		}
		if(ignoreCase) {
			// If characters don't match but case may be ignored,
			// try converting both characters to uppercase.
			// If the results match, then the comparison scan should
			// continue.
			char u1 = toupper(c1);
			char u2 = toupper(c2);
			if(u1 == u2) {
				continue;
			}
			// Unfortunately, conversion to uppercase does not work properly
			// for the Georgian alphabet, which has strange rules about case
			// conversion.  So we need to make one last check before
			// exiting.
			if(tolower(u1) == tolower(u2)) {
				continue;
			}
		}
		return false;
	}
	return true;
}