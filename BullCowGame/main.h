/*
*	Created by: Dan
*	Created: 09/07/2017
*
*	Last Modified By: Dan
*	Last Modified: 16/07/2017
*
*	Description: Header file for main.cpp
*/
#pragma once
#include "BullCowGame.h"
#include <string>

using FText = std::string;
using int32 = int;

class Main {
	public:
		void start();
		bool playAgain();

	private:
		void getGuess(int32 num);
		void printGuess(int32 num);
		void getValidGuesses();
		void startChoices();
		void options();
		void difOptions();
		void setGuessDif();
		void setWordDif();
		void quit();
		void playGame();
		void summariseGame();
		bool matchN(FText ynString);
		bool matchY(FText ynString);
		FText guess;
		FText tempInput;
		FText getGuessDif();
		FText getWordDif();
		BullCowGame bCG;

};