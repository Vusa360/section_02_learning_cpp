/*
*	Created by: Dan
*	Created: 09/07/2017
*
*	Last Modified By: Dan
*	Last Modified: 16/07/2017
*
*	Description: Simple class to make use of BullCowGame class
*	and allow user to play the game through the device's local
*	console
*/

#include "main.h"
#include "BullCowGame.h"
#include <iostream>
#include <string>
#include <ctime>
#include <regex>
#include <windows.h>

using FText = std::string;
using int32 = int;

void Main::start() {
	std::cout	<< "                                                _..._       .-'''-.                                                                                   " << std::endl
				<< "                     .---..---.              .-'_..._''.   '   _    \\                                                                                 " << std::endl
				<< "  /|                 |   ||   |            .' .'      '.\\/   /` '.   \\                                            __  __   ___         __.....__      " << std::endl
				<< "  ||                 |   ||   |           / .'          .   |     \\  '       _     _            .--./)           |  |/  `.'   `.   .-''         '.    " << std::endl
				<< "  ||                 |   ||   |          . '            |   '      |  '/\\    \\\\   //           /.''\\\\            |   .-.  .-.   ' /     .-''\"'-.  `.  " << std::endl
				<< "  ||  __             |   ||   |          | |            \\    \\     / / `\\\\  //\\\\ //           | |  | |      __   |  |  |  |  |  |/     /________\\   \\ " << std::endl
				<< "  ||/'__ '.   _    _ |   ||   |          | |             `.   ` ..' /    \\`//  \\'/             \\`-' /    .:--.'. |  |  |  |  |  ||                  | " << std::endl
				<< "  |:/`  '. ' | '  / ||   ||   |          . '                '-...-'`      \\|   |/              /(\"'`    / |   \\ ||  |  |  |  |  |\\    .-------------' " << std::endl
				<< "  ||     | |.' | .' ||   ||   |           \\ '.          .                  '                   \\ '---.  `\" __ | ||  |  |  |  |  | \\    '-.____...---. " << std::endl
				<< "  ||\\    / '/  | /  ||   ||   |            '. `._____.-'/                                       /'\"\"'.\\  .'.''| ||__|  |__|  |__|  `.             .'  " << std::endl
				<< "  |/\\'..' /|   `'.  |'---''---'              `-.______ /                                       ||     ||/ /   | |_                   `''-...... -'    " << std::endl
				<< "  '  `'-'` '   .'|  '/                                `                                        \\'. __// \\ \\._,\\ '/                                    " << std::endl
				<< "            `-'  `--'                                                                           `'---'   `--'  `\"                                     " << std::endl;


	std::cout << "Welcome to Bulls and Cows, a fun word game" << std::endl;

	startChoices();
}

void Main::startChoices() {
	std::cout << std::endl << "Please select one of the options below by typing 1, 2, or 3" << std::endl
		<< "1) Play" << std::endl
		<< "2) Change Difficulty" << std::endl
		<< "3) Quit" << std::endl << std::endl;

	std::getline(std::cin, tempInput);
	if(tempInput.compare("1") == 0)
		playGame();
	else if(tempInput.compare("2") == 0)
		options();
	else if(tempInput.compare("3") == 0)
		quit();
	else
		startChoices();
}

void Main::options() {
	difOptions();
	start();
}

void Main::difOptions() {
	std::cout << std::endl << "Please select one of the options below by typing 1 or 2" << std::endl
		<< "1) Change the amount of guesses - Current Difficulty: " <<
		getGuessDif() << std::endl
		<< "2) Change the word difficulty - Current Difficulty: " <<
		getWordDif() << std::endl << std::endl;

	std::getline(std::cin, tempInput);
	if(tempInput.compare("1") == 0)
		setGuessDif();
	else if(tempInput.compare("2") == 0)
		setWordDif();
	else
		difOptions();
}

void Main::setGuessDif() {
	std::cout << std::endl << "Select one of the options below by typing 1, 2, or 3 - Current Difficulty: " << 
		getGuessDif() << std::endl
		<< "1) Lots of Guesses" << std::endl
		<< "2) Some Guesses" << std::endl
		<< "3) Few Guesses" << std::endl << std::endl;

	std::getline(std::cin, tempInput);
	if(tempInput.compare("1") == 0)
		bCG.setGuessDif(Dificulty::EASY);
	else if(tempInput.compare("2") == 0)
		bCG.setGuessDif(Dificulty::MEDIUM);
	else if(tempInput.compare("3") == 0)
		bCG.setGuessDif(Dificulty::HARD);
	else
		setGuessDif();
}

void Main::setWordDif() {
	std::cout << std::endl << "Select one of the options below by typing 1, 2, or 3 - Current Difficulty: " <<
		getWordDif() << std::endl
		<< "1) Common Words" << std::endl
		<< "2) Rare Words" << std::endl
		<< "3) Unheard of Words" << std::endl << std::endl;

	std::getline(std::cin, tempInput);
	if(tempInput.compare("1") == 0)
		bCG.setWordDif(Dificulty::EASY);
	else if(tempInput.compare("2") == 0)
		bCG.setWordDif(Dificulty::MEDIUM);
	else if(tempInput.compare("3") == 0)
		bCG.setWordDif(Dificulty::HARD);
	else
		setWordDif();
}

FText Main::getGuessDif() {
	switch(bCG.getGuessDif()) {
		case Dificulty::EASY:
			return "Lots of Guesses";
		case Dificulty::MEDIUM:
			return "Some Guesses";
		case Dificulty::HARD:
			return "Few Guesses";
		default:
			return "Lots of Guesses";
	}
}

FText Main::getWordDif() {
	switch(bCG.getWordDif()) {
		case Dificulty::EASY:
			return "Common Words";
		case Dificulty::MEDIUM:
			return "Rare Words";
		case Dificulty::HARD:
			return "Unheard of Words";
		default:
			return "Common Words";
	}
}

void Main::quit() {
	PostMessage(GetConsoleWindow(), WM_CLOSE, 0, 0);
}

void Main::getGuess(int32 g) {
	std::cout << "Enter guess (" << g << " of " << bCG.getMaxTries() << "): ";
	std::getline(std::cin, guess);
}

void Main::printGuess(int32 g) {
	std::cout << "Guess " << g << " was: " << guess << std::endl;
}

bool Main::matchN(FText input) {
	const std::regex n("^n$", std::regex_constants::ECMAScript | std::regex_constants::icase);
	const std::regex no("^no$", std::regex_constants::ECMAScript | std::regex_constants::icase);

	if (!input.empty())
		if (std::regex_match(input, n) || std::regex_match(input, no))
			return true;

	return false;
}

bool Main::matchY(FText input) {
	const std::regex y("^y$", std::regex_constants::ECMAScript | std::regex_constants::icase);
	const std::regex yes("^yes$", std::regex_constants::ECMAScript | std::regex_constants::icase);

	if (!input.empty())
		if (std::regex_match(input, y) || std::regex_match(input, yes))
			return true;

	return false;
}

bool Main::playAgain() {
	FText ans = "";
	std::cout << std::endl << "Do you want to play again (Y/N): ";
	std::getline(std::cin, ans);

	if(matchY(ans)) {
		std::cout << std::endl;
		system("cls");
		return true;
	} else if (matchN(ans))
		return false;
	else {
		std::cout << "Please enter Y or N" << std::endl;
		return playAgain();
	}

	return false;
}

void Main::getValidGuesses() {
	int32 curTry = bCG.getCurrentTry();

	while(curTry <= bCG.getMaxTries()) {
		if(bCG.isGameWon()) {
			return;
		}
		std::cout << std::endl;
		getGuess(bCG.getCurrentTry());

		GuessValidity valid = bCG.isValidGuess(guess);

		if(valid == GuessValidity::OK) {
			BullCowCount bCC = bCG.submitValidGuess(guess);
			std::cout << "Guess " << curTry << " had " << bCC.bulls << " Bulls and " << bCC.cows << " Cows" << std::endl;
		} else if(valid == GuessValidity::INCORRECT_LENGTH)  {
			std::cout << "Please enter a valid guess - Ensure your guess is the right length" << std::endl;
		} else if(valid == GuessValidity::NONE_ALPHA_CHARS) {
			std::cout << "Please enter a valid guess - Ensure your guess only contains alpha characters (A - Z) (a - z)" << std::endl;
		} else if(valid == GuessValidity::NOT_ISOGRAM) {
			std::cout << "Please enter a valid guess - Ensure your guess is an isogram (ie. it has no repeating letters)" << std::endl;
		}

		curTry = bCG.getCurrentTry();
	}
}

void Main::playGame() {
	bCG.reset();
	std::cout << std::endl << "Welcome to Bulls and Cows, a fun word game" << std::endl
		<< "Can you guess the " << bCG.getHiddenWord().length()
		<< " letter word I am thinking of in " << bCG.getMaxTries() << " guesses" << std::endl;
	getValidGuesses();
	summariseGame();
}

void Main::summariseGame() {
	if(bCG.isGameWon())
		std::cout << std::endl << "Congratulations on winning the game" << std::endl;
	else
		std::cout << std::endl << "Better luck next time - The word was: " << bCG.getHiddenWord() << std::endl;
}

int main() {
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);

	MoveWindow(console, r.left, r.top, 1250, r.bottom, TRUE);

	Main m;
	do
		m.start();
	while (m.playAgain());
	PostMessage(console, WM_CLOSE, 0, 0);
	return 0;
}