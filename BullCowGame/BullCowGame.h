/*
*	Created by: Dan
*	Created: 11/07/2017
*
*	Last Modified By: Dan
*	Last Modified: 16/07/2017
*
*	Description: Header file for BullCowGame.cpp
*/

#pragma once
#include <string>
#include <vector>

using FString = std::string;
using int32 = int;

struct BullCowCount {
	int32 bulls = 0;
	int32 cows = 0;
};

enum class Dificulty {
	EASY,
	MEDIUM,
	HARD
};

enum class GuessValidity {
	OK,
	NOT_ISOGRAM,
	INCORRECT_LENGTH,
	NONE_ALPHA_CHARS
};

class BullCowGame {
	public:
		BullCowGame();

		int32 getMaxTries() const;
		int32 getCurrentTry() const;
		bool isGameWon() const;
		GuessValidity isValidGuess(FString) const;

		void reset();
		void setWordDif(Dificulty);
		void setGuessDif(Dificulty);

		BullCowCount submitValidGuess(FString);
		FString getHiddenWord();

		Dificulty getWordDif();
		Dificulty getGuessDif();

	private:
		void defineDictionaries();
		int32 currentTry;
		int32 maxTries;
		int32 calcMaxTries(FString, Dificulty);
		FString hiddenWord;
		FString convertToLowerCase(FString);
		bool gameWon;
		FString chooseWord(std::vector<FString>);
		std::vector<FString> easyDic;
		std::vector<FString> medDic;
		std::vector<FString> hardDic;
		Dificulty wordDif;
		Dificulty guessDif;
		const Dificulty STARTING_GUESS_DIFFICULTY = Dificulty::EASY;
		const Dificulty STARTING_WORD_DIFFICULTY = Dificulty::EASY;
};